
package GEA.gea.controllers;

import GEA.gea.entities.Compte;
import GEA.gea.repository.compteRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/compte")
@CrossOrigin("*")
public class CompteController {
    
    @Autowired
    compteRepository compteRepository;
    
    @PostMapping(value = "/save")
    public Compte save(@RequestBody Compte compte){
        return compteRepository.save(compte);
    }
    
    @PostMapping(value = "/load")
    public Compte load(@RequestBody final Compte compte){
        compteRepository.save(compte);
        return compteRepository.findByLogin(compte.getLogin());
    }
    
    @GetMapping(value = "/all")
    public List<Compte> findAll(){
        return compteRepository.findAll();
    }
    
    @GetMapping(value = "username/{login}")
    public Compte findByLogin(@PathVariable String login){
        return compteRepository.findByLogin(login);
    }
    
    @GetMapping(value = "identifiant/{password}")
    public Compte findByPassword(@PathVariable String password){
        return compteRepository.findByPassword(password);
    }
    
    @GetMapping(value = "level/{niveau}")
    public Compte findByNiveau(@PathVariable String niveau){
        return compteRepository.findByNiveau(niveau);
    }
    
    @GetMapping(value = "date/{dateCreation}")
    public Compte findBydateCreation(@PathVariable Date dateCreation){
        return compteRepository.findBydateCreation(dateCreation);
    }
    
    @GetMapping(value = "dateConnexion/{dernierConnexion}")
    public Compte findBydernierConnexion(@PathVariable Date dernierConnexion){
        return compteRepository.findBydernierConnexion(dernierConnexion);
    }
    
    @DeleteMapping(value = "/delete")
    public void delete(){
        compteRepository.deleteAll();
    }
    
    @Transactional
    @DeleteMapping(value = "remove/{idCompte}")
    public Long deleteByIdCompte(@PathVariable Long idCompte){
        return compteRepository.deleteByIdCompte(idCompte);
    }
    
    @PutMapping(value = "update/{idCompte}")
    public Compte Update(@PathVariable Long idCompte, @RequestBody Compte compte){
        return compteRepository.save(compte);
    }
}
