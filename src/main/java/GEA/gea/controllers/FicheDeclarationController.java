
package GEA.gea.controllers;

import GEA.gea.entities.FicheDeclaration;
import GEA.gea.repository.ficheDeclarationRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ficheDeclaration")
@CrossOrigin("*")
public class FicheDeclarationController {
    
    @Autowired
    ficheDeclarationRepository ficheDeclarationRepositry;
    
    @PostMapping(value = "/save")
    public <Save extends FicheDeclaration> Save save(@RequestBody Save ficheDeclaration){
        return ficheDeclarationRepositry.save(ficheDeclaration);
    }
    
    @PostMapping(value = "/load")
    public <Load extends FicheDeclaration> Load findByIdFicheDeclaration(@RequestBody final Load ficheDeclaration){
        ficheDeclarationRepositry.save(ficheDeclaration);
        return ficheDeclarationRepositry.findByIdFicheDeclaration(ficheDeclaration.getIdFicheDeclaration());
    }
    
    @GetMapping(value = "/all")
    public List<FicheDeclaration> findAll(){
        return ficheDeclarationRepositry.findAll();
    }
    
    @GetMapping(value = "dateCreation/{date}")
    public Optional<FicheDeclaration> findByDate(@PathVariable Date date){
        return ficheDeclarationRepositry.findByDate(date);
    }
    
    @GetMapping(value = "nomAgence/{agence}")
    public Optional<FicheDeclaration> findByAgence(@PathVariable String agence){
        return ficheDeclarationRepositry.findByAgence(agence);
    }
    
    @GetMapping(value = "designationEnvoi/{designation}")
    public Optional<FicheDeclaration> findByDesignation(@PathVariable String designation){
        return ficheDeclarationRepositry.findByDesignation(designation);
    }
    
    @GetMapping(value = "periodeEnvoi/{periode}")
    public Optional<FicheDeclaration> findByPeriode(@PathVariable Date periode){
        return ficheDeclarationRepositry.findByPeriode(periode);
    }
    
    @GetMapping(value = "quantiteEnvoi/{quantite}")
    public Optional<FicheDeclaration> findByQuantite(@PathVariable int quantite){
        return ficheDeclarationRepositry.findByQuantite(quantite);
    }
    
    @GetMapping(value = "destinationReception/{destination}")
    public Optional<FicheDeclaration> findByDestinationReception(@PathVariable String destination){
        return ficheDeclarationRepositry.findByDestination(destination);
    }
    
    @GetMapping(value = "observationFinale/{observation}")
    public Optional<FicheDeclaration> findByObservation(@PathVariable String observation){
        return ficheDeclarationRepositry.findByObservation(observation);
    }
    
    @DeleteMapping(value = "/delete")
    public void deleteAll(){
        ficheDeclarationRepositry.deleteAll();
    }
    
    @Transactional
    @DeleteMapping(value = "remove/{idFicheDeclaration}")
    public Long deleteByIdFicheDeclaration(@PathVariable Long idFicheDeclaration){
        return ficheDeclarationRepositry.deleteByIdFicheDeclaration(idFicheDeclaration);
    }
    
    @PutMapping(value = "update/{idFicheDeclaration}")
    public <Update extends FicheDeclaration> Update save(@RequestBody Update ficheDeclaration, @PathVariable Long idFicheDeclaration){
        return ficheDeclarationRepositry.save(ficheDeclaration);
    }
    
}
