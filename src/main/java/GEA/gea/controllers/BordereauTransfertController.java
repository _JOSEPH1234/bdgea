
package GEA.gea.controllers;

import GEA.gea.entities.BordereauTransfert;
import GEA.gea.repository.bordereauTransfertRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bordereauTransfert")
@CrossOrigin("*")
public class BordereauTransfertController {
    
    @Autowired
    bordereauTransfertRepository bordereauTransfertRepository;
    
    @PostMapping(value = "/save")
    public <Save extends BordereauTransfert> Save save(@RequestBody Save bordereauTransfert){
        return bordereauTransfertRepository.save(bordereauTransfert);
    }
    
    @PostMapping(value = "/load")
    public <Load extends BordereauTransfert> Load findByIdBordereauTransfert(@RequestBody Load bordereauTransfert){
        bordereauTransfertRepository.save(bordereauTransfert);
        return bordereauTransfertRepository.findByIdBordereauTransfert(bordereauTransfert.getIdBordereauTransfert());
    }
    
    @GetMapping(value = "/all")
    public List<BordereauTransfert> findAll(){
        return bordereauTransfertRepository.findAll();
    }
    
    @GetMapping(value = "dateCreation/{date}")
    public Optional<BordereauTransfert> findByBordereauTransfert(@PathVariable Date date){
        return bordereauTransfertRepository.findByDate(date);
    }
    
    @GetMapping(value = "agence/{agenceProvenanceService}")
    public Optional<BordereauTransfert> findByAgenceProvenanceService(@PathVariable String agenceProvenanceService){
        return bordereauTransfertRepository.findByAgenceProvenanceService(agenceProvenanceService);
    }
    
    @GetMapping(value = "nombreDeBoite/{nombreBoite}")
    public Optional<BordereauTransfert> findByNombreBoite(@PathVariable int nombreBoite){
        return bordereauTransfertRepository.findByNombreBoite(nombreBoite);
    }
    
    @GetMapping(value = "periodeCreation/{periode}")
    public Optional<BordereauTransfert> findByPeriode(@PathVariable Date periode){
        return bordereauTransfertRepository.findByPeriode(periode);
    }
    
    @GetMapping(value = "destinationFinal/{destination}")
    public Optional<BordereauTransfert> findByDestination(@PathVariable String destination){
        return bordereauTransfertRepository.findByDestination(destination);
    }
    
    @GetMapping(value = "nomResponsableCommercial/{nomRespCcialControlInterne}")
    public Optional<BordereauTransfert> findByNomRespCcialControlInterne(@PathVariable String nomRespCcialControlInterne){
        return bordereauTransfertRepository.findByNomRespCcialControlInterne(nomRespCcialControlInterne);
    }
    
    @GetMapping(value = "nomResponsableAgence/{nomRespAgenceService}")
    public Optional<BordereauTransfert> findByNomRespAgenceService(@PathVariable String nomRespAgenceService){
        return bordereauTransfertRepository.findByNomRespAgenceService(nomRespAgenceService);
    }
    
    @GetMapping(value = "chargeMagasinier/{nomMagasinier}")
    public Optional<BordereauTransfert> findByNomMagasinier(@PathVariable String nomMagasinier){
        return bordereauTransfertRepository.findByNomMagasinier(nomMagasinier);
    }
    
    @DeleteMapping(value = "/delete")
    public void deleteAll(){
        bordereauTransfertRepository.deleteAll();
    }
    
    @Transactional
    @DeleteMapping(value = "remove/{idBordereauTransfert}")
    public Long deleteByIdBordereauTransfert(@PathVariable Long idBordereauTransfert){
        return bordereauTransfertRepository.deleteByIdBordereauTransfert(idBordereauTransfert);
    }
    
    @PostMapping(value = "update/{idBordereauTransfert}")
    public <Update extends BordereauTransfert> Update save(@RequestBody Update bordereauTransfert, @PathVariable Long idBordereauTransfert){
        return bordereauTransfertRepository.save(bordereauTransfert);
    }
}
