
package GEA.gea.controllers;

import GEA.gea.entities.Agence;
import GEA.gea.repository.agenceRepository;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/agence")
@CrossOrigin("*")
public class AgenceController {
    
    @Autowired
    private agenceRepository agenceRepository;
    
    @GetMapping(value = "/all")  
    public List<Agence> findAll(){
        return agenceRepository.findAll(); 
    }
    
    @GetMapping(value = "code/{codeAgence}")
    public Optional<Agence> findByCodeAgence(@PathVariable String codeAgence){
        return agenceRepository.findByCodeAgence(codeAgence);
    }
    
    @GetMapping(value = "name/{nomAgence}")
    public Optional<Agence> findByName(@PathVariable String nomAgence){
        return agenceRepository.findByNomAgence(nomAgence);
    }
    
    @GetMapping(value = "chief/{chefAgence}")
    public List<Agence> findByChefAgenceLike(@PathVariable String chefAgence){
        return agenceRepository.findByChefAgenceLike("%" + chefAgence + "%");
    }
    
    @GetMapping(value = "chief_s/{chefAgence}")
    public Optional<Agence> findByChefAgence(@PathVariable String chefAgence){
        return agenceRepository.findByChefAgence(chefAgence);
    }
    
    @PostMapping(value = "/save"/*, consumes = "application/json", produces = "application/json"*/)
    public <Save extends Agence> Save save(@RequestBody Save agence){
        return agenceRepository.save(agence);
    }
    
    @PostMapping(value = "/load")
    public Optional<Agence> load(@RequestBody final Agence agence){
        agenceRepository.save(agence);
        return agenceRepository.findByNomAgence(agence.getNomAgence());
    }
    
    @Transactional
    @DeleteMapping(value = "/{codeAgence}")
    public long deleteByCode(@PathVariable String codeAgence){
        return agenceRepository.deleteByCodeAgence(codeAgence);
    }
    
    @PutMapping(value = "update/{codeAgence}"/*, consumes = "application/json", produces = "application/json"*/)
    public <Update extends Agence> Update modify(@RequestBody Update agence, @PathVariable Long codeAgence){
        return agenceRepository.save(agence);
    }

}
