
package GEA.gea.controllers;

import GEA.gea.entities.Employe;
import GEA.gea.repository.employeRepository;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employe")
@CrossOrigin("*")
public class EmployeController {
    
    @Autowired
    employeRepository employeRepository;
    
    @PostMapping(value = "/save")
    public <Save extends Employe> Save save(@RequestBody Save employe){
        return employeRepository.save(employe);
    }
    
    @PostMapping(value = "/load")
    public Employe load(@RequestBody final Employe employe){
        employeRepository.save(employe);
        return employeRepository.findByMatricule(employe.getMatricule());
    }
    
    @GetMapping(value = "/all")
    public List<Employe> findAll(){
        return employeRepository.findAll();
    }
    
    @GetMapping(value = "firstname/{nomEmploye}")
    public Employe findByNomEmploye(@PathVariable String nomEmploye){
        return employeRepository.findByNomEmploye(nomEmploye);
    }
    
    @GetMapping(value = "lastname/{prenomEmploye}")
    public Employe findByPrenomEmploye(@PathVariable String prenomEmploye){
        return employeRepository.findByPrenomEmploye(prenomEmploye);
    }
    
    @GetMapping(value = "identifiant/{matricule}")
    public Employe findByMatricule(@PathVariable String matricule){
        return employeRepository.findByMatricule(matricule);
    }
    
    @DeleteMapping(value = "/delete")
    public void deleteAll(){
        employeRepository.deleteAll();
    }
    
    @Transactional
    @DeleteMapping(value = "remove/{idEmploye}")
        public Long deleteByIdEmploye(@PathVariable Long idEmploye){
        return employeRepository.deleteByIdEmploye(idEmploye);
    }
    
    @PutMapping(value = "update/{idEmploye}")
    public <Update extends Employe> Update modify(@RequestBody Update employe, @PathVariable Long idEmploye){
        return employeRepository.save(employe);
    }
}