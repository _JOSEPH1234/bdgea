
package GEA.gea.controllers;

import GEA.gea.entities.Departement;
import GEA.gea.repository.departementRepository;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/departement")
@CrossOrigin("*")
public class DepartementController {
    
    @Autowired
    departementRepository departementRepository;
    
    @GetMapping(value = "/all")
    public List<Departement> findAll(){
        return departementRepository.findAll();
    }
    
    @GetMapping(value = "name/{nomDepartement}")
    public <Optional> Departement findByNomDepartement(@PathVariable String nomDepartement){
        return departementRepository.findByNomDepartement(nomDepartement);
    }
    
    @GetMapping(value = "chief/{chefDepartement}")
    public <Optional> Departement findByChefDepartement(@PathVariable String chefDepartement){
        return departementRepository.findByChefDepartement(chefDepartement);
    }
    
    @PostMapping(value = "/save")
    public <Save extends Departement> Save save(@RequestBody Save departement){
        return departementRepository.save(departement);
    }
    
    @PutMapping(value = "update/{idDepartement}")
    public <Update extends Departement> Update save(@RequestBody Update departement, @PathVariable Long idDepartement){
        return departementRepository.save(departement);
    }
    
    @PostMapping(value = "/load")
    public Departement load(@RequestBody final Departement departement){
        departementRepository.save(departement);
        return departementRepository.findByNomDepartement(departement.getNomDepartement());
    }
    
    @DeleteMapping(value = "/delete")
    public void delete(){
        departementRepository.deleteAll();
    }
    
    @Transactional
    @DeleteMapping(value = "remove/{idDepartement}")
    public long deleteByIdDepartement(@PathVariable Long idDepartement){
        return departementRepository.deleteByIdDepartement(idDepartement);
    }
}
