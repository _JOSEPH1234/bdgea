
package GEA.gea.controllers;

import GEA.gea.entities.Archive;
import GEA.gea.repository.archiveRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/archive")
@CrossOrigin("*")
public class ArchiveController {
    
    @Autowired
    archiveRepository archiveRepository;
    
    @PostMapping(value = "/save")
    public <Save extends Archive> Save save(Save archive){
        return archiveRepository.save(archive);
    }
    
    @PostMapping(value = "/load")
    public <Load extends Archive> Load load(@RequestBody final Load archive){
        archiveRepository.save(archive);
        return archiveRepository.findByIdArchive(archive.getIdArchive());
    }
    
    @GetMapping(value = "/all")
    public List<Archive> findByAll(){
        return archiveRepository.findAll();
    }
    
    @GetMapping(value = "dureeVie/{periodeVie}")
    public Optional<Archive> findByPeriodeVie(@RequestBody Archive archive, @PathVariable Date periodeVie){
        return archiveRepository.findByPeriodeVie(periodeVie);
    }
    
    @DeleteMapping(value = "/delete")
    public void deleteAll(){
        archiveRepository.deleteAll();
    }
    
    @Transactional
    @DeleteMapping(value = "remove/{idArchive}")
    public Long deleteByIdArchive(@PathVariable Long idArchive){
        return archiveRepository.deleteByIdArchive(idArchive);
    }
    
    @PostMapping(value = "update/{idArchive}")
    public <Update extends Archive> Update save(@RequestBody Update archive, @PathVariable Long idArchive){
        return archiveRepository.save(archive);
    }
}
