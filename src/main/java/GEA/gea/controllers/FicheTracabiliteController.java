
package GEA.gea.controllers;

import GEA.gea.entities.FicheTracabilite;
import GEA.gea.repository.ficheTracabiliteRepository;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ficheTracabilite")
@CrossOrigin("*")
public class FicheTracabiliteController {
    
    @Autowired
    ficheTracabiliteRepository ficheTracabiliteRepository;
    
    @PostMapping(value = "/save")
    public <Save extends FicheTracabilite> Save save(@RequestBody Save ficheTracabilte){
        return ficheTracabiliteRepository.save(ficheTracabilte);
    }
    
    @PostMapping(value = "/load")
    public <Load extends FicheTracabilite> Load findByIdFicheTracabilite(@RequestBody final Load ficheTracabilte){
        ficheTracabiliteRepository.save(ficheTracabilte);
        return ficheTracabiliteRepository.findByIdFicheTracabilite(ficheTracabilte.getIdFicheTracabilite());
    }
    
    @GetMapping(value = "/all")
    public List<FicheTracabilite> findAll(){
        return ficheTracabiliteRepository.findAll();
    }
    
    @GetMapping(value = "dateCreation/{date}")
    public Optional<FicheTracabilite> findByDate(@PathVariable Date date){
        return ficheTracabiliteRepository.findByDate(date);
    }
    
    @GetMapping(value = "agenceEnvoi/{agence}")
    public Optional<FicheTracabilite> findByAgence(@PathVariable String agence){
        return ficheTracabiliteRepository.findByAgence(agence);
    }
    
    @GetMapping(value = "typeOperationEffectue/{typeOperation}")
    public Optional<FicheTracabilite> findByTypeOperation(@PathVariable String typeOperation){
        return ficheTracabiliteRepository.findByTypeOperation(typeOperation);
    }
    
    @GetMapping(value = "designationEnvoi/{designation}")
    public Optional<FicheTracabilite> findByDesignation(@PathVariable String designation){
        return ficheTracabiliteRepository.findByDesignation(designation);
    }
    
    @GetMapping(value = "periodeEnvoi/{periode}")
    public Optional<FicheTracabilite> findByPeriode(@PathVariable Date periode){
        return ficheTracabiliteRepository.findByPeriode(periode);
    }
    
    @GetMapping(value = "quantiteEnvoye/{quantite}")
    public Optional<FicheTracabilite> findByQuantite(@PathVariable int quantite){
        return ficheTracabiliteRepository.findByQuantite(quantite);
    }
    
    @GetMapping(value = "destinationReception/{destination}")
    public Optional<FicheTracabilite> findByDestination(@PathVariable String destination){
        return ficheTracabiliteRepository.findByDestination(destination);
    }
    
    @GetMapping(value = "observationFinale/{observation}")
    public Optional<FicheTracabilite> findByObservation(@PathVariable String observation){
        return ficheTracabiliteRepository.findByObservation(observation);
    }
    
    @DeleteMapping(value = "/delete")
    public void deleteAll(){
        ficheTracabiliteRepository.deleteAll();
    }
    
    @Transactional
    @DeleteMapping(value = "remove/{idFicheTracabilite}")
    public Long deleteByIdFicheTracabilite(@PathVariable Long idFicheTracabilite){
        return ficheTracabiliteRepository.deleteByIdFicheTracabilite(idFicheTracabilite);
    }
    
    @PutMapping(value = "update/{idFicheTracabilite}")
    public <Update extends FicheTracabilite> Update save(@RequestBody Update ficheTracabilite, @PathVariable Long idFicheTracabilite){
        return ficheTracabiliteRepository.save(ficheTracabilite);
    }   
}