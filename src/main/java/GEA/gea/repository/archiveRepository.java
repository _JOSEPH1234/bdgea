
package GEA.gea.repository;

import GEA.gea.entities.Archive;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface archiveRepository extends JpaRepository<Archive, Long>{

    public Optional<Archive> findByPeriodeVie(Date periodeVie);

    public <Load extends Archive> Load findByIdArchive(Long idArchive);

    public Long deleteByIdArchive(Long idArchive);
    
}
