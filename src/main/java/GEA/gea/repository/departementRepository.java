
package GEA.gea.repository;

import GEA.gea.entities.Departement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface departementRepository extends JpaRepository<Departement, Long>{

    public Departement findByNomDepartement(String nomDepartement);

    public Departement findByChefDepartement(String chefDepartement);

    public long deleteByIdDepartement(Long idDepartement);
    
}
