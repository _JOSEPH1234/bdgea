
package GEA.gea.repository;

import GEA.gea.entities.Compte;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;

public interface compteRepository extends JpaRepository<Compte, Long>{

    public Compte findByLogin(String login);

    public Compte findByPassword(String password);

    public Compte findByNiveau(String niveau);

    public Compte findBydateCreation(Date dateCreation);

    public Compte findBydernierConnexion(Date dernierConnexion);

    public Long deleteByIdCompte(Long idCompte);
    
}
