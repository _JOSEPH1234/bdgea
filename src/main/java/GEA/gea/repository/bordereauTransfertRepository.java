
package GEA.gea.repository;

import GEA.gea.entities.BordereauTransfert;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface bordereauTransfertRepository extends JpaRepository<BordereauTransfert, Long>{

    public Optional<BordereauTransfert> findByDate(Date date);

    public Optional<BordereauTransfert> findByAgenceProvenanceService(String agenceProvenanceService);

    public Optional<BordereauTransfert> findByNombreBoite(int nombreBoite);

    public Optional<BordereauTransfert> findByPeriode(Date periode);

    public Optional<BordereauTransfert> findByDestination(String destination);

    public Optional<BordereauTransfert> findByNomRespCcialControlInterne(String nomRespCcialControlInterne);

    public Optional<BordereauTransfert> findByNomRespAgenceService(String nomRespAgenceService);

    public Optional<BordereauTransfert> findByNomMagasinier(String nomMagasinier);

    public <Load extends BordereauTransfert> Load findByIdBordereauTransfert(Long idBordereauTransfert);

    public Long deleteByIdBordereauTransfert(Long idBordereauTransfert);
    
}
