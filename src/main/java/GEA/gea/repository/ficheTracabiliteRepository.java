
package GEA.gea.repository;

import GEA.gea.entities.FicheTracabilite;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ficheTracabiliteRepository extends JpaRepository<FicheTracabilite, Long>{

    public Optional<FicheTracabilite> findByDate(Date date);

    public Optional<FicheTracabilite> findByAgence(String agence);

    public Optional<FicheTracabilite> findByTypeOperation(String typeOperation);

    public Optional<FicheTracabilite> findByDesignation(String designation);

    public Optional<FicheTracabilite> findByPeriode(Date periode);

    public Optional<FicheTracabilite> findByQuantite(int quantite);

    public Optional<FicheTracabilite> findByDestination(String destination);

    public Optional<FicheTracabilite> findByObservation(String observation);

    public <Load extends FicheTracabilite> Load findByIdFicheTracabilite(Long idFicheTracabilite);

    public Long deleteByIdFicheTracabilite(Long idFicheTracabilite);
    
}
