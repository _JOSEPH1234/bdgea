
package GEA.gea.repository;

import GEA.gea.entities.Employe;
import org.springframework.data.jpa.repository.JpaRepository;

public interface employeRepository extends JpaRepository<Employe, Long>{

    public Employe findByNomEmploye(String nomEmploye);

    public Employe findByPrenomEmploye(String prenomEmploye);

    public Employe findByMatricule(String matricule);

    public Long deleteByIdEmploye(Long idEmploye);
    
}
