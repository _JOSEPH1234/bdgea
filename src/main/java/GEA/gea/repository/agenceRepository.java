
package GEA.gea.repository;

import GEA.gea.entities.Agence;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface agenceRepository extends JpaRepository<Agence, Long>{

    Optional<Agence> findByNomAgence(String nomAgence);

    public long deleteByCodeAgence(String codeAgence);

    Optional<Agence> findByCodeAgence(String codeAgence);

    Optional<Agence> findByChefAgence(String chefAgence);

    public List<Agence> findByChefAgenceLike(String string);
    
}