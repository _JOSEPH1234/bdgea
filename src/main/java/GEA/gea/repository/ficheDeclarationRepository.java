
package GEA.gea.repository;

import GEA.gea.entities.FicheDeclaration;
import java.util.Date;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ficheDeclarationRepository extends JpaRepository<FicheDeclaration, Long>{

    public Optional<FicheDeclaration> findByDate(Date date);

    public Optional<FicheDeclaration> findByAgence(String agence);

    public Optional<FicheDeclaration> findByDesignation(String designation);

    public Optional<FicheDeclaration> findByPeriode(Date periode);

    public Optional<FicheDeclaration> findByQuantite(int quantite);

    public Optional<FicheDeclaration> findByDestination(String destination);

    public Optional<FicheDeclaration> findByObservation(String observation);

    public <Load extends FicheDeclaration> Load findByIdFicheDeclaration(Long idFicheDeclaration);

    public Long deleteByIdFicheDeclaration(Long idFicheDeclaration);
    
}
