
package GEA.gea.entities;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "departement")
@Data
public class Departement implements Serializable {

    @Id @GeneratedValue
    private Long idDepartement;
    
    @OneToMany(mappedBy = "fkd")
    private Set<Employe> employe;
    
    @Column(nullable = false)
    private String nomDepartement;
    
    @Column(nullable = false)
    private String chefDepartement;
    
}
