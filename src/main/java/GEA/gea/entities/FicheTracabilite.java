
package GEA.gea.entities;

import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

@Entity
@Table(name = "ficheTracabilite")
@Data
public class FicheTracabilite {
    
    @Id @GeneratedValue
    private Long idFicheTracabilite;
    
    @OneToMany(mappedBy = "fkt", cascade = CascadeType.REMOVE)
    private Set<Archive> archive;
    
    @Temporal(TemporalType.DATE) @Column(name = "date", nullable = false) 
    private Date date;
    
    @Column(nullable = false)
    private String agence;
    
    @Column(nullable = false)
    private String typeOperation;
    
    @Column(nullable = false)
    private String designation;
    
    @Temporal(TemporalType.DATE) @Column(name = "periode", nullable = false)
    private Date periode;
    
    @Column(nullable = false)
    private int quantite;
    
    @Column(nullable = false)
    private String destination;
    
    @Column(nullable = false)
    private String observation;
    
}
