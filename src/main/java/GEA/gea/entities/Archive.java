
package GEA.gea.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

@Entity
@Table(name = "archive")
@Data
public class Archive {
    
    @Id @GeneratedValue
    private Long idArchive;
    
    @ManyToOne
    private Employe fk;
    
    @ManyToOne
    private FicheDeclaration fkd;
    
    @ManyToOne
    private FicheTracabilite fkt;
    
    @ManyToOne
    private BordereauTransfert fkb;
    
    @Temporal(TemporalType.TIMESTAMP) @Column(name = "periodeVie", nullable = false, insertable = false, updatable = false) 
    private Date periodeVie;
}
