
package GEA.gea.entities;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "agence")
@Data
public class Agence implements Serializable {
    
    @Id @GeneratedValue
    private Long idAgence;
    
    @OneToMany(mappedBy = "fka")
    private Set<Employe> employe; 
    
    @Column(nullable = false)
    private String nomAgence;
    
    @Column(nullable = false, unique = true)
    private String codeAgence;
    
    @Column(nullable = false)
    private String chefAgence;

    @Override
    public String toString(){
         return "Agence [idAgence = " + idAgence + ", nomAgence = " + nomAgence + "codeAgence = " + codeAgence + " chefAgence = " + chefAgence + "]";
    }
    
}
