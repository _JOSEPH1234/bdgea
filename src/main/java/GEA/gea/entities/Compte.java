
package GEA.gea.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Data;

@Entity
@Table(name = "compte")
@Data
public class Compte implements Serializable{
    
    @Id @GeneratedValue
    private Long idCompte;
    
    @ManyToOne(cascade = CascadeType.REMOVE)
    private Employe fk;
    
    @Column(nullable = false, unique = true)
    private String login;
    
    @Column(nullable = false)
    private String password;
    
    @Column(nullable = false)
    private String niveau;
    
    @Temporal(TemporalType.TIMESTAMP) @Column(name = "dateCreation", nullable = false, insertable = false, updatable = false) 
    private Date dateCreation;
    
    @Temporal(TemporalType.TIMESTAMP) @Column(name = "dateDerniereCreation", nullable = false, insertable = false, updatable = false) 
    private Date dernierConnexion;
    
}
