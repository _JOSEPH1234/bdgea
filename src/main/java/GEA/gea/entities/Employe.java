
package GEA.gea.entities;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "employe")
@Data
public class Employe {
    
    @Id @GeneratedValue
    private Long idEmploye;
    
    @ManyToOne
    private Departement fkd;
    
    @ManyToOne
    private Agence fka;
    
    @OneToMany(mappedBy = "fk")
    private Set<Compte> compte;
    
    @Column(nullable = false)
    private String nomEmploye;
    
    @Column(nullable = false)
    private String prenomEmploye;
    
    @Column(nullable = false, unique = true)
    private String matricule;
    
    @Column(nullable = false)
    private boolean user;

}
