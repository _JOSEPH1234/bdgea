package GEA.gea;

import GEA.gea.entities.Agence;
import GEA.gea.repository.agenceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeaApplication implements CommandLineRunner {
    
    @Autowired
    agenceRepository agc;
    
    public static void main(String[] args) {
        SpringApplication.run(GeaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        
        // agc.save(new Agence("aakhd", "tata", "pqbdb"));
    }


}
